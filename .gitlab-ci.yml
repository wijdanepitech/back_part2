stages:
  - installing
  - building
  - testing
  - backing_up
  - deploying

variables:
  NODE_VERSION: '8'
  PHP_VERSION: '7.3'
  COMPOSER_VERSION: '2.6.6'

before_script:
  - >-
    echo "Using Node ${NODE_VERSION} for Frontend and PHP ${PHP_VERSION} for Backend"

install_front:
  stage: installing
  image: 'node:$NODE_VERSION'
  cache:
    key: ${CI_COMMIT_REF_SLUG}-node-modules
    paths:
      - front/node_modules/
  script:
    - cd front
    - npm install
  artifacts:
    paths:
      - front/node_modules/

install_back:
  stage: installing
  image: 'php:$PHP_VERSION'
  cache:
    key: ${CI_COMMIT_REF_SLUG}-vendor
    paths:
      - back/vendor/
  before_script:
    - apt-get update && apt-get install -y unzip git zip zlib1g-dev libzip-dev
    - docker-php-ext-install zip
    - echo "zend_extension=/usr/local/lib/php/extensions/no-debug-non-zts-20190902/xdebug.so" >> /usr/local/etc/php/php.ini
    - curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
  script:
    - cd back
    - composer install --prefer-dist --no-interaction
  artifacts:
    paths:
      - back/vendor/
      - back/.env

build_front:
  stage: building
  image: 'node:$NODE_VERSION'
  script:
    - cd front
    - 'npm run build:prod'
  artifacts:
    paths:
      - front/dist/

build_back:
  stage: building
  image: 'php:$PHP_VERSION'
  script:
    - cd back

  artifacts:
    paths:
      - back/vendor/
      - back/.env

backing_frontend:
  stage: backing_up
  image: alpine:latest
  before_script:
    - apk update && apk add --no-cache bash zip openssh-client
  script:
    - echo "Backing up Frontend..."
    - echo -e "$BACKVMKEY" > ./Back_key.pem
    - chmod 600 ./Back_key.pem
    - ssh -o StrictHostKeyChecking=no -i ./Back_key.pem $SSH_HOST 'mv /home/ubuntu/front /home/ubuntu/front_old || true'
    - ssh -o StrictHostKeyChecking=no -i ./Back_key.pem $SSH_HOST 'mkdir -p /home/ubuntu/front'
    - tar -czvf front.tar.gz front/dist/
    - scp -o StrictHostKeyChecking=no -i ./Back_key.pem front.tar.gz $SSH_HOST:/home/ubuntu/
    - ssh -o StrictHostKeyChecking=no -i ./Back_key.pem $SSH_HOST 'tar -xzvf /home/ubuntu/front.tar.gz -C /home/ubuntu/ '
    - rm -f ./Back_key.pem
  dependencies:
    - build_front

backing_backend:
  stage: backing_up
  image: alpine:latest
  before_script:
    - apk update && apk add --no-cache bash zip openssh-client
  script:
    - set -x
    - echo "Backing up Backend..."
    - echo -e "$SSH_PRIVATE_KEY" > ./Back_key.pem
    - chmod 600 ./deploy_key.pem
    - ssh -o StrictHostKeyChecking=no -i ./Back_key.pem.pem $SSH_HOST 'mv /home/ubuntu/back /home/ubuntu/back_old || true'
    - ssh -o StrictHostKeyChecking=no -i ./Back_key.pem $SSH_HOST 'mkdir -p /home/ubuntu/'
    - tar -czvf back.tar.gz back/
    - scp -o StrictHostKeyChecking=no -i ./Back_key.pem back.tar.gz $SSH_HOST:/home/ubuntu/
    - ssh -o StrictHostKeyChecking=no -i ./Back_key.pem $SSH_HOST 'tar -xzvf /home/ubuntu/back.tar.gz -C /home/ubuntu/'
    - rm -f ./Back_key.pem
  dependencies:
    - build_back

rollback:
  stage: backing_up
  image: 'ubuntu:latest'
  script:
    - echo "Rolling back Frontend..."
    - ssh -i ./Back_key.pem $SSH_HOST 'mv /home/ubuntu/front /home/ubuntu/front_temp && mv /home/ubuntu/front_old /home/ubuntu/front && mv /home/ubuntu/front_temp /home/ubuntu/front_old'
    - echo "Rolling back Backend..."
    - ssh -i ./Back_key.pem $SSH_HOST 'mv /home/ubuntu/back /home/ubuntu/back_temp && mv /home/ubuntu/back_old /home/ubuntu/back && mv /home/ubuntu/back_temp /home/ubuntu/back_old'
  when: manual

trigger_deploy:
  stage: deploying
  image: alpine:latest
  script:
    - echo "Preparing SSH Key..."
    - echo "$BACKVMKEY" | tr -d '\r' > ./Back_key.pem
    - chmod 600 ./deploy_key.pem
    - echo "Installing SSH Client..."
    - apk add --no-cache openssh-client
    - echo "Running Ansible Playbooks..."
    - |
      ssh -o StrictHostKeyChecking=no -i ./Back_key.pem $SSH_HOST << EOF
        ansible-playbook -i /home/ubuntu/ansible/inventory.ini /home/ubuntu/ansible/database.yml
        ansible-playbook -i /home/ubuntu/ansible/inventory.ini /home/ubuntu/ansible/back.yml
        ansible-playbook -i /home/ubuntu/ansible/inventory.ini /home/ubuntu/ansible/front.yml
      EOF
  when: manual
